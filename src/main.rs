use std::collections::{HashMap, HashSet};
use std::fs::File;
use std::io::{BufRead, BufReader, Lines};

fn main() {
    part_one();
    // part_two();
}

fn part_one() {
    let priorities = init_priorities();
    let mut sum = 0;

    for line_results in read_file("input.txt") {
        let line = line_results.unwrap();

        let left = &line[..line.len() / 2];
        let right = &line[line.len() / 2..];

        let left_set: HashSet<char> = left.chars().collect();
        for r in right.chars().collect::<HashSet<char>>() {
            if left_set.contains(&r) {
                sum += priorities.get(&r).copied().unwrap();
                break;
            }
        }
    }
    println!("{}", sum);
}

fn part_two() {
    let priorities = init_priorities();
    let mut sum = 0;
    let mut n = 0;
    let mut input: [String; 3] = Default::default();

    for line_results in read_file("input.txt") {
        if n == 3 {
            let priority = priorities.get(&get_common(&input).unwrap()).copied().unwrap();
            sum += priority;
            n = 0;
        }
        else {
            input[n] = line_results.unwrap();
            n += 1;
        }
    }
    println!("{}", sum);
}

fn get_common(input: &[String; 3]) -> Option<char> {
    let first: HashSet<char> = input[0].chars().collect();
    let second: HashSet<char> = input[1].chars().collect();
    for th in input[2].chars().collect::<HashSet<char>>() {
        if first.contains(&th) && second.contains(&th) {
            return Some(th);
        }
    }
    None
}

fn read_file(filename: &str) -> Lines<BufReader<File>> {
    let file = File::open(filename).unwrap();
    let reader = BufReader::new(file);
    reader.lines()
}

fn init_priorities() -> HashMap<char, u32> {
    let mut priorities: HashMap<char, u32> = HashMap::with_capacity(52);
    for i in 1..27 {
        priorities.insert(char::from_u32(0x61 + i - 1).unwrap(), i);
    }
    for i in 27..53 {
        priorities.insert(char::from_u32(0x41 + i - 27).unwrap(), i);
    }
    priorities
}